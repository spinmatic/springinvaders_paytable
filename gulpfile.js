const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const browserSync = require('browser-sync').create();

gulp.task("pug", () => {
  return gulp.src("build/pug/*.pug")
      .pipe(pug())
      .pipe(gulp.dest("dist/"))
});

gulp.task("style", () => {
  return gulp.src("build/sass/**/*.sass")
      .pipe(sass())
      .pipe(cleanCss())
      .pipe(autoprefixer())
      .pipe(gulp.dest("dist/assets/style/"))
});


gulp.task("watch", () => {
  gulp.watch(["build/pug/*.pug"],  gulp.series("pug"));
  gulp.watch(["build/sass/**/*sass"],  gulp.series("style"));
});


gulp.task('reload', function() {
  browserSync.init({
    server: {
      index: "index.html",
      baseDir: "dist/"
    }
  });
});

gulp.task("default", gulp.parallel(["pug", "style", "watch", "reload"]));
