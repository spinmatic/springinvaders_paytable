var paytable = {
  data: {
    activePage: 1,
    distSymbols: 0,
    distPaylines: 0
  },
  ui: {
    symbols: document.getElementById("symbols"),
    paylines: document.getElementById("paylines"),
    featuresBtn: document.getElementById("featuresBtn"),
    symbolsBtn: document.getElementById("symbolsBtn"),
    paylinesBtn: document.getElementById("paylinesBtn"),
    playBtn: document.getElementById("playBtn"),
    container: document.querySelector(".container")
  },
  scrollbar: null,
  init: function () {
    this.scrollbar = Scrollbar.init(this.ui.container, {autoHide: false});
    window.scrollTo(0, 0);
    this.initListeners();
  },
  setLanguage(lang) {
    var elements = document.querySelectorAll("[data-lang]");
    var len = elements.length;
    for (var i = 0; i < len; i++) {
      var property = elements[i].getAttribute("data-lang");
      if (lang[property]) {
        elements[i].innerHTML = lang[property];
      }
    }
  },
  setActiveElement: function (elm) {
    switch (elm) {
      case 1:
        this.ui.featuresBtn.classList.add("active");
        this.ui.symbolsBtn.classList.remove("active");
        this.ui.paylinesBtn.classList.remove("active");
        break;
      case 2:
        this.ui.featuresBtn.classList.remove("active");
        this.ui.symbolsBtn.classList.add("active");
        this.ui.paylinesBtn.classList.remove("active");
        break;
      case 3:
        this.ui.featuresBtn.classList.remove("active");
        this.ui.symbolsBtn.classList.remove("active");
        this.ui.paylinesBtn.classList.add("active");
        break;
    }
  },
  initListeners: function () {
    this.scrollbar.addListener(this.scrollEvent.bind(this));
    this.ui.playBtn.addEventListener("click", this.close);
    this.ui.playBtn.addEventListener("touchend", this.close);

    document.querySelectorAll("header .buttons a").forEach(anchorLink => {
      anchorLink.addEventListener("click", this.handleAnchorChange.bind(this, anchorLink));
    });

    window.addEventListener("orientationchange", this.orientationChangeEvent.bind(this));
    window.addEventListener("message", (function (e) {
      switch (e.data.action) {
        case "language":
          this.setLanguage(e.data.payload);
          break;
        case "open":
          this.getInitialOffsetValues();
      }
    }).bind(this));
    document.addEventListener("DOMContentLoaded", function () {
      if (window.parent) {
        window.parent.postMessage("requestLanguageData", "*");
      }
    });
  },
  handleAnchorChange: function (anchor, event) {
    const hashArr = anchor.href.split("#") || [];

    const hash = hashArr.length > 0
        ? hashArr[1]
        : null;

    if (hash)
      this.scrollbar.scrollTo(0, document.getElementById(hash).offsetTop, 1000)

  },
  close: function () {
    if (window.parent) {
      window.parent.postMessage("closePaytable", "*");
    }
  },
  scrollEvent: function (state) {
    this.data.distSymbols = this.ui.symbols.offsetTop;
    this.data.distPaylines = this.ui.paylines.offsetTop;

    let containerHeight = this.ui.container.getElementsByClassName("section-holder")[0].offsetHeight;
    let scrollBottomAmount = containerHeight - (window.innerHeight + 46);
    let distance = this.scrollbar.scrollTop;

    if (distance > (this.data.distPaylines - 25) || distance > scrollBottomAmount) {
      this.setActiveElement(3);
      this.data.activePage = 3;
    } else if (distance > (this.data.distSymbols - 25)) {
      this.setActiveElement(2);
      this.data.activePage = 2;
    } else {
      this.setActiveElement(1);
      this.data.activePage = 1;
    }

  },
  orientationChangeEvent: function () {
    if (this.data.activePage === 1) {
      this.ui.container.scrollTo(0, 0);
    } else if (this.data.activePage === 2) {
      this.ui.container.scrollTo(0, this.data.distSymbols);
    } else if (this.data.activePage === 3) {
      this.ui.container.scrollTo(0, this.data.distPaylines);
    }
  },
  getInitialOffsetValues: function () {
    var self = this;
    var iteration = 0;
    var interval = setInterval(function () {
      iteration++;
      if (self.data.distSymbols === 0 || self.data.distPaylines === 0) {
        var symbolsOffsetTop = self.ui.symbols.offsetTop;
        var paylinesOffsetTop = self.ui.paylines.offsetTop;
        if (symbolsOffsetTop !== 0)
          self.data.distSymbols = symbolsOffsetTop;
        if (paylinesOffsetTop !== 0)
          self.data.distPaylines = paylinesOffsetTop;
      } else {
        clearInterval(interval);
      }
      if (iteration === 10)
        clearInterval(interval);
    }, 250);
  },
};

paytable.init();

